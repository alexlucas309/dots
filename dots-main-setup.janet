#!/usr/bin/env janet

(def username (string/trim (invoke "whoami")))
(def cwd (string (os/cwd) "/"))
(def user-home (->. (invoke "getent" "passwd" username) (string/trim) (string/split ":") (string (5 .) "/")))

(defn dot [a b] (os/symlink (string cwd a) (string user-home b)))

# zsh
(->. (os/dir "/home/me") (filter (fn [x] (string/has-prefix? ".zsh" x))) (each f . (os/rm (string user-home f))))
(each f (os/dir "zsh") (dot (string "zsh/" f) (string "." f)))
(envex "sh" "-c" (string "echo " password " | sudo -S chsh " username " -s /usr/bin/zsh")) # change this to use pipes directly

# tmux
(dot "tmux/tmux.conf" ".tmux.conf")

# neovim
(os/mkdir (string user-home ".config/nvim"))
(dot "vim/init.vim" ".config/nvim/init.vim")

# Do this bit for them, eventually.
(print "All that's left is to set up keepsake. You can do that bit manually, I think.")
