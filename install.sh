#!/bin/sh

ensure="$(find $HOME -name 'EnSureCommand.sh' 2> /dev/null | head -1)"

printf "Password for %s: " "$USER"
read -s -r PASS

# install misc packages needed for configs
$ensure cowsay "$PASS" || exit 1
$ensure fortune "$PASS" &&
$ensure dialog "$PASS" &&
$ensure tmux "$PASS" &&
$ensure zsh "$PASS" &&
$ensure curl "$PASS" &&
$ensure sudo "$PASS" &&
$ensure neovim "$PASS"

# install packages to build Janet
$ensure git "$PASS"
$ensure gcc "$PASS"
$ensure make "$PASS"

#install rust and commandline tools
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --profile minimal
. "$HOME/.cargo/env"
cargo install fd-find eza ripgrep bat watchexec-cli
export PATH="${PATH}:$HOME/.cargo/bin/"

# install Janet
tmpdir=/tmp/janet_git_"$(date +%s)"
git clone https://github.com/janet-lang/janet.git "$tmpdir"

save_dir=$(pwd)
cd "$tmpdir" || {
    echo "failed to change to $tmpdir"
    exit 2
}

sed -i "s/PREFIX?=\/usr\/local/PREFIX?=$(echo $HOME | sed 's/\//\\\//g')\/\.local/" Makefile

make
make test
make install
make install-jpm-git

rm -rf "$tmpdir"
cd "$save_dir" || {
    echo "failed to change to $save_dir"
    exit 3
}

# install prelude to Janet path
ln -s "$(find "$HOME" -name prelude.janet 2> /dev/null)" "$HOME/.local/lib/janet/"

# kickoff the rest of setup to Janet
export PATH="${PATH}:$HOME/.local/bin/"

cd "$(dirname "$0")" || {
    echo "failed to change to $(dirname "$0")"
    exit 4
}

janet -e "(def password \"$PASS\")" -l prelude dots-main-setup.janet

exit $?
