#!/usr/bin/env janet

(use prelude)

(def tmux-active (invoke "pidof" "tmux"))

# Just attaches to the first free session
# (if (not tmux-active)
#   (envex "tmux" "new")
#   ((def sessions (invoke "tmux" "ls"))
#     (def available (find (fn [x] (not (string/find ") (attached)" x))) (string/split "\n" sessions)))
#     (if (not (empty? available))
#       (envex "tmux" "attach" "-t" (0 (string/split ":" available)))
#       (envex "tmux" "new"))
#     (os/exit 0)))

(if (not tmux-active) (do (envex "tmux" "new") (os/exit 0)))

(def sessions (string/split "\n" (string/trim (invoke "tmux" "ls"))))
(def available (filter (fn [x] (not (string/find ") (attached)" x))) sessions))

(cond (empty? available) (envex "tmux" "new")
  (single? available) (envex "tmux" "attach" "-t" (0 (string/split ":" (0 available))))
  (do
    (def new-marker "+")
    (def choice (invoke-err "dialog" "--cursor-off-label" "--menu" "Available sessions:" "0" "0" "0" new-marker "New session!"
                                ;(flatten (map (fn [s] (string/split ": " s)) available))))
    (cond (= (string choice) new-marker) (envex "tmux" "new")
      (not (nil? choice)) (envex "tmux" "attach" "-t" (string choice)))))

# A man with one watch knows what time it is.
# A man with two watches is never quite sure.

