#!/bin/sh

ensure="$(find $HOME -name 'EnSureCommand.sh' 2> /dev/null | head -1)"

echo $ensure

nukeafter=false

if ! [ "$ensure" ]; then
    wget https://gitlab.com/alexlucas309/dots/-/raw/master/util/EnSureCommand.sh
    chmod +x EnSureCommand.sh
    nukeafter=true
    ensure="$(pwd)/EnSureCommand.sh"
fi

$ensure git &&
$ensure dialog &&
$ensure gcc &&
$ensure make &&
$ensure tmux &&

if [ "$nukeafter" ]; then
    rm EnSureCommand.sh
fi

tmpdir=/tmp/janet_git_"$(date +%s)"
git clone https://github.com/janet-lang/janet.git "$tmpdir"
echo "cloned"

save_dir=$(pwd)
cd "$tmpdir" || {
    echo "failed to change to $tmpdir"
    exit 2
}
echo "in $tmpdir"

make
make test
sudo make install
sudo make install-jpm-git

sudo rm -rf "$tmpdir"
cd "$save_dir" || {
    echo "failed to change to $save_dir"
    exit 3
}

mkdir -p "$HOME/.local/src/"

cd "$HOME/.local/src/" || {
    echo "failed to change to $HOME/.local/src/"
    exit 4
}

dotsdir="$HOME/.local/src/acl309_dots_$(date +%s)"
git clone https://gitlab.com/alexlucas309/dots.git "$dotsdir"

sudo ln -s "$dotsdir/preludes/janet/prelude.janet" "/usr/local/lib/janet/"

printf "\n\nThe keepsake.janet script is at $dotsdir/term/keepsake.janet;
configure your terminal emulator to run it at startup.
Read $dotsdir/term/README.md for more information.\n\n"

