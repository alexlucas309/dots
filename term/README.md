You can configure you system to use `keepsake.janet` by running the `keepify.sh` script in this directory and then following step 4 below.

To _manually_ configure your system to use `keepsake.janet`, follow these steps.

 1. Install [Janet](https://github.com/janet-lang/janet) and make sure it's in your path. The best way is to clone the repository and build the interpreter from source. You can edit the `Makefile` variable "`PREFIX?`" to change the installation location- I reccomend `$HOME/.local`.
 2. Make sure you have `dialog` installed. This is probably packaged by your distribution as simply "dialog".
 3. Link the `prelude.janet` file- in the `preludes/` directory of this repo -into the `lib/janet/` directory of wherever you installed Janet.
 4. Configure your terminal to execute `keepsake.janet` upon startup instead of your shell. Many terminal emulators (such as Terminator) have an option in their settings for this, but if yours does not, it may still be possible to use it with `keepsake.janet`. If your terminal supports a flag like Xterm's `-e` to execute its argument upon startup, you can edit the `.desktop` file that is used to launch the terminal from a desktop environment. Change the `Exec` line in that file so that it runs `xterm -e /path/to/keepsake.janet`, for example.
