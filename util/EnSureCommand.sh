#!/bin/sh

incmd=""

if [ -e /usr/bin/apt ]
then
    incmd="apt update && apt install"
elif [ -e /usr/bin/pacman ]
then
    incmd="pacman -Syu"
elif [ -e /usr/bin/dnf ]
then
    incmd="dnf in -y"
elif [ -e /usr/bin/yum ]
then
    incmd="yum install"
elif [ -e /usr/bin/xbps ]
then
    incmd="xbps-install --sync --update"
elif [ -e /usr/bin/portage ]
then
    incmd="emerge --ask --verbose --update --deep --newuse @world"
fi

which "$1" 2> /dev/null > /dev/null
found="$?"

which sudo 2> /dev/null > /dev/null
found_sudo="$?"

needed="$1" # Update this at some point to figure out which package
            # to get when the package and executable have different names.

if [ $found = 1 ]; then
    if [ $found_sudo = 0 ]; then
        sh -c "$(if [ "$2" ]; then printf "echo %s |" "$2"; fi) sudo -S $incmd $needed"
    else
        echo "You need sudo to use this script"
        exit 1
    fi
fi

