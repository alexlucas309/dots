#!/usr/bin/env janet

(use prelude)

(def user-shell
  (with [f (file/open "/etc/passwd")]
    (def passwd (file/read f :all))
    (def loc (string/find (^ (os/getenv "USER") ":") passwd))
    (last (string/split ":" (string/slice passwd loc (string/find "\n" passwd loc))))))

(print user-shell)
