(defn any? [& ind] (var state false) (each x ind (if x (set state true))) state)
# (defn deep_replace [target replacement form] (map (fn [x] (cond (eq? target x) replacement (indexed? x) (deep_replace target replacement x) x)) form))
(defn deep_exists? [target ind] (cond (= target ind) true (indexed? ind) (any? ;(map (fn [x] (deep_exists? target x)) ind)) false))

# Make this work for tables and whatnot as well, eventually # apparently it already does
(defmacro as->. [init hole & forms]
  ~(as-> ,init ,hole
    ,;(map |(if (tuple? $0) (if (deep_exists? hole $0) $0 (do (def [op & args] $0) (tuple/join [op] args [hole]))) $0) forms)))

(defmacro as=>. [init hole & forms] ~(set ,init (as->. ,init ,hole ,;forms)))
(defmacro ->. [init & forms] ~(as->. ,init . ,;forms))
(defmacro =>. [init & forms] ~(as=>. ,init . ,;forms))

(defn envex [& args] (os/execute args :p))

(defn invoke [& cmds]
  (let [x (os/spawn cmds :p {:out :pipe}) s (:read (x :out) :all)]
    (:wait x) s))

(defn invoke-err [& cmds]
  (let [x (os/spawn cmds :p {:err :pipe}) s (:read (x :err) :all)]
    (:wait x) s))

(defn single? [xs] (= (length xs) 1))

# Knowing the workings of the universe is no defense against them.

# Implement this better.
(defn flatten-once [xs]
  (defn flattable? [a] (match a [] true [_] true _ false))
  (if (flattable? xs)
    (do (var state [])
      (each x xs (if (flattable? x) (do (set state [;state ;x])) (set state [;state x])))
      state)
    xs))
