let maplocalleader = "\\"

set nu! rnu!
set autoindent
set expandtab
set smarttab
set smartcase

nnoremap * *``
nnoremap # #``

nnoremap <esc> :noh<return><esc>:<backspace>

" set rtp^="/home/al/.opam/default/share/ocp-indent/vim"

syntax on
filetype plugin indent on

set tabstop=4
set shiftwidth=0
set expandtab

