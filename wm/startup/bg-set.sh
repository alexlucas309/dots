#!/bin/bash
trap 'setbg' 10
trap 'ends' 2
setbg () {
    bg_image="$HOME/.background_image"
    if [[ -e "$bg_image" ]]
    then
        feh --bg-fill "$bg_image"
    else
        notify-send "\"$bg_image\" no such file or directory."
    fi
}
ends () {
    rm .rr1 .rr2
    kill $$
}
setbg

#If a file named .rr1 or .rr2 already exists, one of the following two blocks moves it.
if [[ -e ".rr1" ]]
then
    n=1
    while [[ -e ".CONFLICT-rr1-${n}" ]]
    do
        let n=$n+1
        echo $n
        sleep 2
    done
    mv .rr1 .CONFLICT-rr1-${n}
fi

if [[ -e ".rr2" ]]
then
    n=1
    while [[ -e ".CONFLICT-rr2-${n}" ]]
    do
        let n=$n+1
        echo $n
        sleep 2
    done
    mv .rr2 .CONFLICT-rr2-${n}
fi

#Ensure diff doesn't get a "no such file" error
touch .rr1
touch .rr2

while :;
do
    sleep 3&
    f=$!
    if [[ `pgrep xinit` ]]
    then
        if [[ `diff .rr1 .rr2` ]]
        then
            setbg
        fi
    else
        ends
    fi
    mv .rr1 .rr2
    xrandr > .rr1
    wait $f
done
#while sleep 15
#do
#	feh --bg-fill ~/.background_image
#done
