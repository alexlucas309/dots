#!/bin/sh
sleep 0.2
xrandr --output DP-0.3 --primary --mode 2560x1440 --rate 144 --pos 1920x0 --rotate normal --output DP-0 --off --output DP-1 --off --output HDMI-0 --off --output DP-2 --mode 1920x1080 --pos 0x948 --rotate normal --output DP-3 --off --output DP-4 --off
feh --bg-fill ~/.background_image
