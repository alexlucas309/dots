#!/bin/bash
# Autorun this script at window manager startup.

while [[ `pgrep xinit` ]]
do
	sleep 5
done
#Commands to run on stop go below this line.
pkill dbus-daemon
