#!/bin/bash

if [[ $(pidof $1) ]]
then
	pkill $1
else
	$(echo $@| cut -d' ' -f2-)&
fi
