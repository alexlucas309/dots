#!/bin/sh
#eval `xdotool getmouselocation --shell`
vars=()
raw_vars=$(xwininfo -id $(xdotool getactivewindow) | grep -e 'Absolute\|Height\|Width' | awk '{print $NF}')
for q in $raw_vars; do vars=(${vars[@]} ${q}); done
window_upperleft_X=${vars[0]}
window_upperleft_Y=${vars[1]}
window_width=${vars[2]}
window_height=${vars[3]}
coord_X=$(echo "(${window_width}+${window_upperleft_X}*2)/2" | bc)
coord_Y=$(echo "(${window_height}+${window_upperleft_Y}*2)/2" | bc)
xdotool mousemove ${coord_X} ${coord_Y}

#  _________________________________________
# / FORTUNE PROVIDES QUESTIONS FOR THE      \
# | GREAT ANSWERS: #19 A: To be or not to   |
# \ be. Q: What is the square root of 4b^2? /
#  -----------------------------------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||
